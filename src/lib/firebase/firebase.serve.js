import { configSecret } from './config';
import { getApp, initializeApp } from 'firebase-admin/app';
import { getAuth } from 'firebase-admin/auth';
import { getFirestore } from 'firebase-admin/firestore';
import firebaseAdmin from 'firebase-admin';

// firebase-admin is commonjs
const { credential } = firebaseAdmin;

export let firebaseServerApp;
export let firebaseServerAuth;
export let firebaseServerFirestore;

try {
	// try get again
	firebaseServerApp = getApp();
	firebaseServerAuth = getAuth(firebaseServerApp);
	firebaseServerFirestore = getFirestore(firebaseServerApp);
} catch (error) {
	// create new
	firebaseServerApp = initializeApp({
		credential: credential.cert(configSecret.firebaseKey)
	});
	firebaseServerAuth = getAuth(firebaseServerApp);
	firebaseServerFirestore = getFirestore(firebaseServerApp);

	// firestore config
	firebaseServerFirestore.settings({
		ignoreUndefinedProperties: true
	});
}
