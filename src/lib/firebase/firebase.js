// Import the functions you need from the SDKs you need
import { deleteApp, getApp, getApps, initializeApp } from "firebase/app";
import { getFirestore } from 'firebase/firestore'
import { getAuth } from 'firebase/auth'
import { configPublic } from './config';

// Initialize Firebase
// const app = initializeApp(firebaseConfig);

let firebaseApp;

if(!getApps().length) {
  firebaseApp = initializeApp(configPublic.firebaseKey)
} else {
  firebaseApp = getApp()
  deleteApp(firebaseApp)
  firebaseApp = initializeApp(configPublic.firebaseKey)
}

export const db = getFirestore(firebaseApp)
export const auth = getAuth(firebaseApp)