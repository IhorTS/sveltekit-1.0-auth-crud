/** configPublic put config key which need to use in front end, client side */
export const configPublic = {
	firebaseKey: JSON.parse(import.meta.env.VITE_FIREBASE_CLIENT_CONFIG ?? '{}'),
	buildTime: import.meta.env.VITE_BUILD_TIME,
	siteUrl: import.meta.env.VITE_SITE_URL ?? 'http://localhost:3000'
};

/** configSecret put any config value which only used in server and never show with in browser */
export const configSecret = {
	firebaseKey: JSON.parse(import.meta.env.VITE_FIREBASE_SERVER_CONFIG ?? '{}')
};